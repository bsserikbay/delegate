﻿using System;

namespace Event
{
    delegate void AccountStateHandler(string message);

    class Account
    {
        AccountStateHandler _del;
        public event AccountStateHandler Added;
        public event AccountStateHandler Adding;
        public event AccountStateHandler Withdrawn;


        int _amount;

        public Account(int amount)
        {
            _amount = amount;

        }

        public void Put(int amount)
        {
           
            if (Adding != null)
            {
                Adding($"receiving amount {amount}");
            }
            _amount += amount;
            if (Added != null)
            {
                Added($"received amount {amount}");
            }
        }

        public void Withdraw(int amount)
        {
            if (_amount >= amount)
            {
                _amount -= amount;
                if (Withdrawn != null)
                {
                    Withdrawn($"withdrawed amount {amount}");

                }
            }
            else
            {
                if (Withdrawn != null)
                {
                    Withdrawn("insufficient amount");

                }
            }

        }

    }
    class Program
    {
        static void Main(string[] args)
        {
            Account account = new Account(100);
            account.Added += new AccountStateHandler(Display);
            account.Withdrawn += Display;
            account.Withdrawn += ColorDisplay;
            account.Put(200);
            account.Withdraw(100);
            account.Withdrawn -= ColorDisplay;
            account.Withdraw(300);
        }

        public static void Display(string message)
        {
            Console.WriteLine(message);
        }

        public static void ColorDisplay(string message)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(message);
            Console.ResetColor();
        }
    }
}
