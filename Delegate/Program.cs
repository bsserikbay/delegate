﻿using System;

namespace Delegate
{
    class Program
    {
        delegate void Message();
        delegate int Operation(int x, int y);

        static void Main(string[] args)
        {
            Message message = Display;
            // message = new Message(Display);
            // message.Invoke();
            message();

            Operation operation = Multiply;
            
            Console.WriteLine(operation(4, 6));

            DoOperation(4, 5, Add);
            DoOperation(45555, 532323, Add);
        }

        static int Add(int x, int y)
        {
            return x + y;
        }

        static int Multiply(int x, int y)
        {
            return x * y;
        }

        static void Display()
        {
            Console.WriteLine("Hello");
        }

        static void DoOperation(int x, int y, Operation operation)
        {
            int res =  operation(x, y);
            Console.WriteLine(res);
        }

        
    }
}
