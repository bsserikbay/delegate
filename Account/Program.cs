﻿using System;

namespace Account
{
    delegate void AccountStateHandler(string message);

    class Account
    {
        AccountStateHandler _del;

        public void RegisterHandler(AccountStateHandler del)
        {
            _del += del;
        }
        public void UnRegisterHandler(AccountStateHandler del)
        {
            _del -= del;
        }

        int _amount;

        public Account(int amount)
        {
            _amount = amount;

        }

        public void Put(int amount)
        {
            _amount += amount;
            if(_del!= null)
            {
                _del($"received amount {amount}");
            }
        }

        public void Withdraw(int amount)
        {
            if (_amount >= amount)
            {
                _amount -= amount;
                if (_del!= null)
                {
                    _del($"withdrawed amount {amount}");

                }
            }
            else
            {
                if (_del!=null)
                {
                    _del("insufficient amount");

                }
            }

        }

    }


    class Program
    {
        static void Main(string[] args)
        {
            Account account = new Account(100);
            account.RegisterHandler(new AccountStateHandler(Display));
            account.RegisterHandler(ColorDisplay);
            account.Put(200);

            account.Withdraw(100);

            account.UnRegisterHandler(ColorDisplay);
            account.Withdraw(300);
            
        }
        public static void Display(string message)
        {
            Console.WriteLine(message);
        }

        public static void ColorDisplay(string message)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(message);
            Console.ResetColor();
        }
    }
}
